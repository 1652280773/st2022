import paramiko
import time
from influxdb import InfluxDBClient

client = InfluxDBClient('192.168.153.130', 8086)
client.switch_database('monitor')
port = [22, 8050, 8051, 8052]
measurement = ['sever_5s', 'vm1_5s', 'vm2_5s', 'vm3_5s']


def add_data_to_influxdb(fields, measurement):

    content = [
        {
            "measurement": measurement,
            "fields": fields
        }
    ]
    client.write_points(content)


def connectHost(port, ip='10.11.55.80', uname='st2022', passwd='st2022'):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(ip, username=uname, password=passwd, port=port)
    return ssh


def MainCheck():
    # try:
        while True:

            for a in range(len(port)):
                ssh = connectHost(port[a])
                # 查询主机名称
                cmd = 'hostname'
                stdin, stdout, stderr = ssh.exec_command(cmd)
                host_name = stdout.readlines()
                host_name = host_name[0]
                # 查看当前时间
                csj = 'date +%T'
                stdin, stdout, stderr = ssh.exec_command(csj)
                curr_time = stdout.readlines()
                curr_time = curr_time[0]

                # 查看cpu使用率(取三次平均值)
                cpu = "vmstat 1 3|sed  '1d'|sed  '1d'|awk '{print $15}'"
                stdin, stdout, stderr = ssh.exec_command(cpu)
                cpu = stdout.readlines()
                # cpu_usage = str(round((100 - (int(cpu[0]) + int(cpu[1]) + int(cpu[2])) / 3), 2)) + '%'
                cpu_usage = round((100 - (int(cpu[0]) + int(cpu[1]) + int(cpu[2])) / 3), 2)

                # 查看内存使用率
                # mem = "cat /proc/meminfo|sed -n '1,4p'|awk '{print $2}'"
                # stdin, stdout, stderr = ssh.exec_command(mem)
                # mem = stdout.readlines()
                # mem_total = round(int(mem[0]) / 1024)
                # mem_total_free = round(int(mem[1]) / 1024) + round(int(mem[2]) / 1024) + round(int(mem[3]) / 1024)
                # mem_usage = str(round(((mem_total - mem_total_free) / mem_total) * 100, 2)) + "%"
                # mem = "cat /proc/meminfo|sed -n '1,3p'|awk '{print $2}'"
                # stdin, stdout, stderr = ssh.exec_command(mem)
                # mem = stdout.readlines()
                # mem_total = round(int(mem[0]))
                # mem_total_free = round(int(mem[1])) + round(int(mem[2]))
                # # mem_usage = str(abs(round(((mem_total - mem_total_free) / mem_total) * 100, 2))) + "%"
                # mem_usage = abs(round(((mem_total - mem_total_free) / mem_total) * 100, 2))
                mem = "cat /proc/meminfo|sed -n '1,3p'|awk '{print $2}'"
                stdin, stdout, stderr = ssh.exec_command(mem)
                mem = stdout.readlines()
                mem_total = round(int(mem[0]))
                mem_total_free = round(int(mem[1])) + round(int(mem[2]))
                mem_usage = abs(round(((mem_total - mem_total_free) / mem_total) * 100, 2))

                # 查看磁盘使用率
                disk = "df"
                stdin, stdout, stder = ssh.exec_command(disk)
                disk_list = stdout.readlines()
                length = len(disk_list)
                disk_use = 0
                disk_all = 0
                for i in [1, length - 1]:
                    disk_all = disk_all + int(disk_list[i].split()[1])
                    disk_use = disk_use + int(disk_list[i].split()[2])
                # disk_usage = str(round(float(disk_use) / disk_all * 100)) + '%'
                disk_usage = round(float(disk_use) / disk_all * 100)

                #查看网络信息
                net_query = "ip -s link |sed -n '10p;12p'| awk '{print $1}'"
                stdin, stdout, stderr = ssh.exec_command(net_query)
                net1 = stdout.readlines()
                net1_rx = int(net1[0])
                net1_tx = int(net1[1])
                stdin, stdout, stderr = ssh.exec_command(net_query)
                net2 = stdout.readlines()
                net2_rx = int(net2[0])
                net2_tx = int(net2[1])
                # net_rx = str(round((net2_rx - net1_rx) / 1024, 2)) + 'Kbps'
                # net_tx = str(round((net2_tx - net1_tx) / 1024, 2)) + 'Kbps'
                net_rx = round((net2_rx - net1_rx) / 1024, 2)
                net_tx = round((net2_tx - net1_tx) / 1024, 2)
                fields = {
                    'cpu_usage': cpu_usage,
                    'mem_usage': mem_usage,
                    'disk_usage': disk_usage,
                    'net_rx': net_rx,
                    'net_tx': net_tx
                }
                add_data_to_influxdb(fields, measurement[a])
                print(fields)


    # except:
    #     print("连接服务器 %s 异常" % (port[a]))


if __name__ == '__main__':
    MainCheck()

