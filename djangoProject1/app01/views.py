from django.core import serializers
from django.http import HttpResponse
from django.shortcuts import render
from influxdb import InfluxDBClient
from django.core.serializers import serialize
import json
# Create your views here.
from scrapy.utils import console

client = InfluxDBClient('192.168.153.130', 8086)
client.switch_database('monitor')


def sever_5s(req):
    ret = client.query('select*from sever_5s order by time desc limit 10')
    for item in ret:
        return HttpResponse(json.dumps(item), content_type="application/json")


def vm1_5s(req):
    ret = client.query('select*from vm1_5s order by time desc limit 10')
    for item in ret:
        return HttpResponse(json.dumps(item), content_type="application/json")
    return HttpResponse(ret)


def vm2_5s(req):
    ret = client.query('select*from vm2_5s order by time desc limit 10')
    for item in ret:
        return HttpResponse(json.dumps(item), content_type="application/json")
    return HttpResponse(ret)


def vm3_5s(req):
    ret = client.query('select*from vm3_5s order by time desc limit 10')
    for item in ret:
        return HttpResponse(json.dumps(item), content_type="application/json")
    return HttpResponse(ret)


def sever_30m(req):
    ret = client.query('select*from sever_30m order by time desc limit 1')
    for item in ret:
        return HttpResponse(json.dumps(item), content_type="application/json")


def vm1_30m(req):
    ret = client.query('select*from vm1_30m order by time desc limit 1')
    for item in ret:
        return HttpResponse(json.dumps(item), content_type="application/json")
    return HttpResponse(ret)


def vm2_30m(req):
    ret = client.query('select*from vm2_30m order by time desc limit 1')
    for item in ret:
        return HttpResponse(json.dumps(item), content_type="application/json")
    return HttpResponse(ret)


def vm3_30m(req):
    ret = client.query('select*from vm3_30m order by time desc limit 1')
    for item in ret:
        return HttpResponse(json.dumps(item), content_type="application/json")
    return HttpResponse(ret)